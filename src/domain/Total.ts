export interface Total {
    tipoMoneda: string;
    montoInicial: string;
    montoActual: string;
    gananciaParcial: string;
    gananciaTotal: string;
}
