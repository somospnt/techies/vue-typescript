export interface Activo {
    nombre?: string;
    fechaInicial?: Date;
    montoInicial?: string;
    fechaActual?: Date;
    montoActual?: string;
    gananciaParcial?: string;
    gananciaTotal?: string;
    moneda?: string;
    id?: number;
}
